<!-- #include file="cookies.asp" -->	
	
	<html>

	<head>

	<title>Marathon Fund Investor Day 2020</title>

	<link href="generic.css" rel="stylesheet" type="text/css">

	<script language="javascript" type="text/javascript" src="../globalStyles.js"></script>


	<script language="javascript">

	function doSubmit() {
		doValidate();
	}

	function doValidate() {
		var ucomp = document.userdata.company.value;
		var uname = document.userdata.username.value;
		var email = document.userdata.email.value;
		//var uphone = document.userdata.uniqueid.value;
		//var ufax = document.userdata.ContactFirst.value;
		var pword = document.userdata.password.value;
		if ( uname == null || uname == "" || uname.length < 2 ) {
			alert("Please enter your name in the Name field.");
			document.userdata.username.focus();
			return false;
		}
		if ( ucomp == null || ucomp == "" || ucomp.length < 2 ) {
			alert("Please enter your company's name in the Company field.");
			document.userdata.company.focus();
			return false;
		}
		if ( email == null || email == "" || email.length < 2 ) {
			alert("Please enter your e-mail address in the E-mail field.");
			document.userdata.email.focus();
			return false;
		}
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (!re.test(email)) {
			alert("The email address you entered does not appear to be\n" +
				"valid. Please enter your complete email address.");
			document.userdata.email.focus();
			return false;
		}
		/*if ( uphone == null || uphone == "" || uphone.length < 10 ) {
			alert("Please enter your Telephone Number with area code in the Telephone field.");
			document.userdata.phone.focus();
			return false;
		}*/
		/*if ( ufax == null || ufax == "" || ufax.length < 10 ) {
			alert("Please enter your Fax Number with area code in the Fax field.");
			document.userdata.fax.focus();
			return false;
		}*/
		if ( pword == null || pword == "" || pword.length < 2 ) {
			alert("Please enter a valid password.");
			document.userdata.password.focus();
			return false;
		}
		//var foore = """;
		document.userdata.submit();
		return true;
		//
	}

	</SCRIPT>

	</head>

	<body leftmargin="0" topmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="#FFFFFF">
	<br><br>
	<form action="/feynman/protoshow/validate.asp" method="post" name="userdata">
		<table border="0" cellspacing="0" cellpadding="0" align="center" width="80%">
			<tr>
				<td colspan=4 align="center"  class="MaraBluebold12">To enter the Marathon Fund Investor Day 2020, please fill in the following information:<br><br><br></td>
			</tr>
			<tr>
				<td><img src="/nrs/images/main/spacer.gif" height=2 width=40></td>
				<td align="right" class="gray12bold">Name: </td>
				<td><input type="text" name="username" size="30" value="<%=Session("mid2020userName")%>"></td>
				<td width=70>&nbsp;</td>
			</tr>
			<tr>
				<td><img src="/nrs/images/main/spacer.gif" height=2 width=40></td>
				<td align="right" class="gray12bold">Company: </td>
				<td><input type="text" name="company" size="30" value="<%=Session("mid2020userCompany")%>"></td>
				<td width=70>&nbsp;</td>
			</tr>
			<tr>
				<td><img src="/nrs/images/main/spacer.gif" height=2 width=40></td>
				<td align="right" class="gray12bold">Email: </td>
				<td><input type="text" name="email" size="30" value="<%=Session("mid2020userEmail")%>"></td>
				<td width=70>&nbsp;</td>
			</tr>
			<!--<tr>
				<td><img src="/nrs/images/main/spacer.gif" height=2 width=40></td>
				<td align="right" class="gray12bold">Telephone: </td>
				<td><input type="text" name="uniqueid" size="30" value="<%=Session("mid2020userPhone")%>"></td>
				<td width=70>&nbsp;</td>
			</tr>-->
			<!--<tr>
				<td><img src="/nrs/images/main/spacer.gif" height=2 width=40></td>
				<td align="right" class="gray12bold">Fax: </td>
				<td><input type="text" name="ContactFirst" size="30" value="<%=Session("mid2020userFax")%>"></td>
				<td width=70>&nbsp;</td>
			</tr>-->
			<tr>
				<td><img src="/nrs/images/main/spacer.gif" height=2 width=40></td>
				<td align="right" class="gray12bold">Password: </td>
				<td><input type="password" name="password" size="30" value=""></td>
				<td width=70>&nbsp;</td>
			</tr>

			<tr>
				<td colspan=4 align="center"  class="MaraBluebold12"><br><br><br></td>
			</tr>
			
			
			<tr>
				
			</tr>
			<tr>
				<td colspan="2" align="center" valign="middle" width=50%>
					<a href="http://www.marathonfund.com"><!--<img src="images/disagree.gif" border="0" align="middle">--></a>
				</td>
				<td colspan="2" align="center" valign="middle" width=50%>
					<a href="javascript:doSubmit()"><img src="images/continue.png" border="0" align="middle"></a>
				</td>
			</tr>
		</table><br>
	</form>
	</body>

	</html>