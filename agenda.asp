<!-- #include file="cookies.asp" -->

<%

if Session("LoginStatus") <> "Validated" then
	Response.Redirect "index.asp"
else


Set DataConn = Server.CreateObject("ADODB.Connection")
DataConn.ConnectionTimeout = Session("DataConn_ConnectionTimeout")
DataConn.CommandTimeout = Session("DataConn_CommandTimeout")
DataConn.Open Session("DataConn_ConnectionString"), Session("DataConn_RuntimeUserName"), Session("DataConn_RuntimePassword")
Set data = Server.CreateObject("ADODB.Recordset")
Set data.ActiveConnection = DataConn
data.MaxRecords = 1
data.CursorType = 1
data.LockType = 3
data.Source = "ConfAccess"
data.Open

data.AddNew
data("UserName") = Left( userName, 49 )
data("UserCompany") = Left( userCompany, 49 )
data("UserEmail") = Left( userEmail, 49 )
'data("ContactFirstname") = Left( userPhone, 49 )
data("Status") = "Success"
data("StartDate") = Date()
data("StartTime") = Time()
data("EventName") = "Marathon Fund Investor Day 2020"
data("ClientName") = "Marathon Fund"
data.Update

data.Close
DataConn.Close
set data = nothing
set DataConn = nothing

Session("disableRegistration") = true
Session("enforceRegistration") = false
Session("autoRegistration") = false

%>

<html>

<head>

<title>Marathon Investor Day | March 11, 2020</title>

<link href="generic.css" rel="stylesheet" type="text/css">

<script language="javascript">

function checkWin(win) {
	var bPopBlocked = false;
	try {
		if (win == null) bPopBlocked = true;
		else if (typeof(win) == "undefined") bPopBlocked = true;
		else if (win.closed) bPopBlocked = true;
		else if (win.length < 0) bPopBlocked = true;
	}
	catch(e) {
		bPopBlocked = true;
	}
	if (bPopBlocked) alert(
	 "Please disable any pop-up blocking software in your browser\n" +
	 "while viewing the Lexington Partners Annual Meeting. Your pop-up blocker\n" +
	 "appears to be interfering with display of the presentation."
	);
}

var presWin = null;

function goToPres(whatShow){

	document.userdata.password.value = whatShow;

	if (!presWin || presWin.closed ){
		presWin = window.open("temp.html","presWin","toolbar=0,menu=0,scrollbars=0,resizable=1,location=0,width=1600,height=900");
		//setTimeout("checkWin(presWin)", 3000);
	}

	document.userdata.submit();
	presWin.focus();

}
</script>

</head>

<body leftmargin="0" topmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

<table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" bgcolor="#FFFFFF" rowspan="2"><img src="images/blank.gif" width="10" height="70" border="0"></td>
		<td align="center" bgcolor="#FFFFFF" width="100%" class="blue18bold"><span class="heading">Marathon Investor Day | March 11, 2020</span><br></td>
	</tr>
</table>
<BR>
<table cellpadding="0" cellspacing="0" border="0">

	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid1');">View from the Top, by Andrew Rabinowitz, President & COO</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>			
			
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid2');" >Marathon&#39;s Macro Outlook, Investment Strategy, Risk Management</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<td>
					<span class="gray12bold">Louis Hanover</span><span class="lightGray12">, CIO</span><br>
					<span class="gray12bold">Jamie Raboy</span><span class="lightGray12">, Chief Risk Officer</span><br>
					<span class="gray12bold">Bruce Richards</span><span class="lightGray12">, Chairman, CEO</span><br>
				</td>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid3');">Distressed and Special Situations Investing</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<tr>
				<td>
					<span class="gray12bold">Jeffrey Jacob</span><span class="lightGray12">, Head of Distressed Debt Special Situation Credit</span><br>
					<span class="gray12bold">Jason Friedman</span><span class="lightGray12">, MD for Corporate Credit Strategy & Business Development</span><br>
					<span class="gray12bold">Moderated by Saul Burian</span><span class="lightGray12">, MD, Head of Restructuring, Houlihan Lokey</span><br>
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid4');">Global Real Estate Opportunities</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<tr>
				<td>
					<span class="gray12bold">Jonathan Braidley</span><span class="lightGray12">, MD, European Real Estate</span><br>
					<span class="gray12bold">Joseph Griffin</span><span class="lightGray12">, MD, U.S. Real Estate</span><br>
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid5');">Structured Credit</a></td>
			</tr>
			<tr>
				<td colspan="3"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<tr>
				<td>
					<span class="gray12bold">Andrew Springer</span><span class="lightGray12">, Head of Structured Credit</span><br>
					<span class="gray12bold">Edward Cong</span><span class="lightGray12">, MD, Structured Credit</span><br>
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid6');">Emerging Markets</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<tr>
				<td>
					<span class="gray12bold">Gabriel Szpigiel</span><span class="lightGray12">, Head of Emerging Markets</span><br>
					<span class="gray12bold">Andrew Szmulewicz</span><span class="lightGray12">, MD, Emerging Markets</span><br>
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid7');">Corporate Credit</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<tr>
				<td>
					<span class="gray12bold">Andrew Brady</span><span class="lightGray12">, Head of Performing Corporate Credit</span><br>
					
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td rowspan="3"><img src="images/blank.gif" border="0" width="10" height="10"></td>
				<td class=><a class="grayBold16" href="#" onClick="goToPres('Mid2020vid8');">Marathon Investor Day | March 11, 2020</a></td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="10"></td>
			</tr>	
			<tr>
				<td>
					<span class="gray12bold"></span><span class="lightGray12">Please use the above link to view the content in its entirety</span><br>
					
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/blank.gif" border="0" width="20" height="40"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td colspan="3" align="center" bgcolor="#FFFFFF"><img src="images/blank.gif" width="20" height="20"></td>
	</tr>
</table>

<form action="/feynman/protoshow/validate.asp" method="post" name="userdata" target="presWin">
<% if Request.Cookies("NrsSingleLogin.ApplicationCookie") <> "" and Session("login_Email") <> "" then %>
<input type="hidden" name="username" size="30" value="<%=Session("mid2020userName")%>">
<input type="hidden" name="company" size="30" value="<%=Session("mid2020userCompany")%>">
<input type="hidden" name="email" size="30" value="<%=Session("mid2020userEmail")%>">
<!--<input type="hidden" name="uniqueid" size="30" value="<%=Session("mid2020userPhone")%>">-->
<input type="hidden" name="password" size="30" value="">
<% else %>
<input type="hidden" name="username" size="30" value="<%=Session("mid2020userName")%>">
<input type="hidden" name="company" size="30" value="<%=Session("mid2020userCompany")%>">
<input name="email" type="hidden" value = "user@ms.com">
<!--<input type="hidden" name="uniqueid" size="30" value="<%=Session("mid2020userPhone")%>">-->
<input type="hidden" name="password" size="30" value="">
<% end if%>
</form>
</body>

</html>
<%
Session("mid2020uniqueid") = userPhone
end if
%>